import java.util.*;

/**
 * This class represents fractions of form n/d where n and d are long integer
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

    /**
     * Main method. Different tests.
     */
    public static void main(String[] param) {
//      Lfraction n1 = new Lfraction(1,2);
//      Lfraction n2 = new Lfraction(2,3);
//      boolean res = n1.equals(n2);
        Lfraction l = new Lfraction(-625L, 93750L);
        String res = l.toString();
//       java.lang.AssertionError: Wrong sum: <-1/250> + <-1/375>
//               Expected :-1/150
//       Actual   :-625/93750


        System.out.println(res);
    }


    private Long num;
    private Long den;

    /**
     * Constructor.
     *
     * @param a numerator
     * @param b denominator > 0
     */
    public Lfraction(long a, long b) {
        if (b == 0) {
            throw new RuntimeException("Can't divide by zero");
        }
        if (a == 0L) {
            num = 0L;
            den = 1L;
        } else {
            // Sign of a fraction is in numerator
            if (b < 0) {
                a = 0 - a;
                b = Math.abs(b);
            }
            Long gcd = getGcd(a, b);
            num = a / gcd;
            den = b / gcd;
        }

    }

    /**
     * Public method to find greatest common divider
     *
     * @param a numerber 1
     * @param b number 2
     * @return divider
     */
    public static Long getGcd(Long a, Long b) {
        Long min = Math.abs(a) < Math.abs(b) ? Math.abs(a) : Math.abs(b);

        for (Long i = min; i > 1; i--) {
            if (a % i == 0 && b % i == 0)
                return i;
        }
        return 1L;
    }

    /**
     * Public method to access the numerator field.
     *
     * @return numerator
     */
    public long getNumerator() {
        return num;
    }

    /**
     * Public method to access the denominator field.
     *
     * @return denominator
     */
    public long getDenominator() {
        return den;
    }

    /**
     * Conversion to string.
     *
     * @return string representation of the fraction
     */
    @Override
    public String toString() {
        return num + "/" + den;
    }

    /**
     * Equality test.
     *
     * @param m second fraction
     * @return true if fractions this and m are equal
     */
    @Override
    public boolean equals(Object m) {
        if (m instanceof Lfraction) {
            Lfraction lf = new Lfraction(((Lfraction) m).num, ((Lfraction) m).den);
            return this.num.equals(lf.num) && this.den.equals(lf.den);
        } else {
            throw new RuntimeException("Object is not instance of Lfraction");
        }
    }

    /**
     * Hashcode has to be equal for equal fractions.
     *
     * @return hashcode
     */
    @Override
    public int hashCode() {
        return this.num.hashCode() + this.den.hashCode();
    }

    /**
     * Sum of fractions.
     *
     * @param m second addend
     * @return this+m
     */
    public Lfraction plus(Lfraction m) {
        Long a, b;
        if (this.den == m.den) {
            a = this.num + m.num;
            b = this.den;
        } else {
            a = this.num * m.den + m.num * this.den;
            b = this.den * m.den;
        }
        return new Lfraction(a, b);
    }

    /**
     * Multiplication of fractions.
     *
     * @param m second factor
     * @return this*m
     */
    public Lfraction times(Lfraction m) {
        return new Lfraction(this.num * m.num, this.den * m.den);
    }

    /**
     * Inverse of the fraction. n/d becomes d/n.
     *
     * @return inverse of this fraction: 1/this
     */
    public Lfraction inverse() {
        return new Lfraction(this.den, this.num);
    }

    /**
     * Opposite of the fraction. n/d becomes -n/d.
     *
     * @return opposite of this fraction: -this
     */
    public Lfraction opposite() {
        return new Lfraction(-(this.num), this.den);
    }

    /**
     * Difference of fractions.
     *
     * @param m subtrahend
     * @return this-m
     */
    public Lfraction minus(Lfraction m) {
        Long a, b;
        if (this.den == m.den) {
            a = this.num - m.num;
            b = this.den;
        } else {
            a = this.num * m.den - m.num * this.den;
            b = this.den * m.den;
        }
        return new Lfraction(a, b);
    }

    /**
     * Quotient of fractions.
     *
     * @param m divisor
     * @return this/m
     */
    public Lfraction divideBy(Lfraction m) {
        return new Lfraction(this.num * m.den, this.den * m.num);
    }

    /**
     * Comparision of fractions.
     *
     * @param m second fraction
     * @return -1 if this < m; 0 if this==m; 1 if this > m
     */
    @Override
    public int compareTo(Lfraction m) {
        long a = this.num * m.den;
        long b = this.den * m.num;
        if (a > b) {
            return 1;
        } else if (a < b) {
            return -1;
        } else {
            return 0;
        }
    }

    /**
     * Clone of the fraction.
     *
     * @return new fraction equal to this
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return new Lfraction(this.num, this.den);
    }

    /**
     * Integer part of the (improper) fraction.
     *
     * @return integer part of this fraction
     */
    public long integerPart() {
        return Math.floorDiv(this.num, this.den);
    }

    /**
     * Extract fraction part of the (improper) fraction
     * (a proper fraction without the integer part).
     *
     * @return fraction part of this fraction
     */
    public Lfraction fractionPart() {
        if (Math.abs(this.num % this.den) == 0) {
            return new Lfraction(0L, 1L);
        } else if (Math.abs(this.num) <= Math.abs(this.den)) {
            return new Lfraction(this.num, this.den);
        } else {
            return new Lfraction(this.num % this.den, this.den);
        }
    }

    /**
     * Approximate value of the fraction.
     *
     * @return numeric value of this fraction
     */
    public double toDouble() {
        return (double) this.num / (double) this.den;
    }

    /**
     * Double value f presented as a fraction with denominator d > 0.
     *
     * @param f real number
     * @param d positive denominator for the result
     * @return f as an approximate fraction of form n/d
     */
    public static Lfraction toLfraction(double f, long d) {
        return new Lfraction(Math.round(f * d), d);
    }

    /**
     * Conversion from string to the fraction. Accepts strings of form
     * that is defined by the toString method.
     *
     * @param s string form (as produced by toString) of the fraction
     * @return fraction represented by s
     */
    public static Lfraction valueOf(String s) {
        if (s != null && s.length() > 0 && s.contains("/")) {
            String[] arr = s.split("/");
            return new Lfraction(Long.parseLong(arr[0]), Long.parseLong(arr[1]));

        } else {
            throw new RuntimeException("Invalid input " + s);
        }
    }
}

